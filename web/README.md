# HTML5 Boilerplate

My personal front-end template for building sites based on [h5bp](https://github.com/h5bp/html5-boilerplate)
I wanted it to be lean and mean and that's why I stripped a lot by default.

* Source: [https://github.com/ischagast/html5-boilerplate](https://github.com/ischagast/html5-boilerplate)
* Personal homepage: [http://ischagast.nl](http://ischagast.nl)
* Twitter: [@IschaGast](http://twitter.com/ischagast)

## Grunt Sass
The only thing that's need to be installed manually is Grunt Sass
sudo npm install --save-dev grunt-sass