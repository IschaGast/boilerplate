# SASS

## Comments
Don't use CSS somments in Sass because they will be inserted into the CSS file and you don't want that.
Use this style of commenting in your scss files
````scss
// **************************************************
//
//  First Level
//  -> Description
//
// **************************************************

// --------------------------------------------------
//  Second Level
// --------------------------------------------------

// Third Level
````

## Placeholders
Luckily Sass 3.2 added a feature called placeholders. Placeholders are selectors that output nothing unless they are extended. Here’s what a placeholder looks like:
````scss
%separator {
    border-top: 1px solid black;
}
hr {
    @extend %separator;
}
.separator {
    @extend %separator;
}
````

That would generate the following CSS:
````scss
hr,
.separator {
    border-top: 1px solid black;
}
````
Thing is, you don’t want to have to repeat .media all over your HTML. Especially because you’re already going to be repeating .status and .profile. That’s where using placeholders makes things awesomely DRY. Here’s our %media pattern:
````scss
%media {
    overflow: hidden;
    &:first-child {
        float: left;
    }
    &:last-child {
        overflow: hidden;
    }
}
````
Now instead of having to repeat .media on all of your elements, you just extend the%media pattern anywhere you want to use it:
````scss
.status {
    @extend %media;
    // Status-specific styles here...
}
.profile {
    @extend %media
    // Profile-specific styles here...
}
````