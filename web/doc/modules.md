# Modules

## Module box
```HTML
<div class="m-box no-border right is-disabled">
    <div class="m-box--body">
        <h2 class="m-box--header">Heading element</h2>
    </div><!-- /m-box--body -->
</div><!-- /m-box -->

<div class="m-box_attention">
    <div class="m-box--body">
        <h2 class="m-box--header">Heading element</h2>
    </div><!-- /m-box--body -->
</div><!-- /m-box -->
```

```CSS
/* m-box module */
.m-box {
}
    /* components */
    .m-box--header { }
    .m-box--body { }

/* modifiers */
.m-box.no-border {
    border: none
}
.m-box.right {
    float: right
}
.m-box.is-disabled {
    background-color: #ccc
}

/* submodule */
.m-box_attention {
  @extend .m-box
  border: 2px solid red
}
```

## Pagination