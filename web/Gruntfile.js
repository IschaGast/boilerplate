module.exports = function(grunt) {

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        grunticon: {
            myIcons: {
                files: [{
                    expand: true,
                    cwd: 'static/svg',
                    src: ['*.svg', '*.png'],
                    dest: "static/css/icons"
                }],
                options: {
                    enhanceSVG: true
                }
            }
        },

        imageoptim: {
            myTask: {
                src: ['img', 'static/img']
            }
        },

        sass: {
            options: {
                sourceMap: true
            },
            dist: {
                files: {
                    'style.css': 'style.scss'
                }
            }
        },

        postcss: {
            options: {
                map: true,
                processors: [
                require('autoprefixer-core')({browsers: 'last 2 version'}),
                ]
            },
            dist: {
                src: 'static/css/*.css',
            }
        },

        patternprimer: {
            options: {
                wwwroot: '/',
                css: ['dist/css/style.css'],
                dest: 'styleguide/docs',
                index: 'styleguide/index.html'
            },
            live: {
                ports: [7020, 7030],
                src: 'styleguide/snippets'
            },
            snapshot: {
                src: 'styleguide/snippets',
                snapshot: true
            }
        },

        watch: {
            css: {
                files: ['static/scss/**/*.scss'],
                tasks: ['sass', 'postcss', 'scsslint'],
                options: {
                    spawn: false
                }
            },
            imageoptim: {
                files: ['img/*.{png,jpg,gif}', 'static/img/*.{png,jpg,gif}'],
                tasks: ['imageoptim'],
                options: {
                    spawn: false
                }
            },
            svg: {
                files: ['static/svg/**/*.svg'],
                tasks: ['grunticon'],
                options: {
                    spawn: false
                }
            }
        }

    });

    grunt.loadNpmTasks('grunt-grunticon');          // https://github.com/filamentgroup/grunticon
    grunt.loadNpmTasks('grunt-contrib-sass');       // https://github.com/gruntjs/grunt-contrib-sass
    grunt.loadNpmTasks('grunt-autoprefixer');       // https://github.com/nDmitry/grunt-autoprefixer
    grunt.loadNpmTasks('grunt-contrib-watch');      // https://github.com/gruntjs/grunt-contrib-watch
    grunt.registerTask('default', ['watch']);

};