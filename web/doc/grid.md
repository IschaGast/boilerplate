# Grid

## Default grid system
Most used is the 960 grid system that uses a 12 column grid, I used the 12 column grid for this example.
```HTML
<div class="row">
    <div class="col_4"></div>
    <div class="col_8"></div>
</div><!-- /row -->
```

## Offsetting columns
When you want to move columns to the right you can use .offset* classes. Each class increases the left margin of a column by a whole column. For example, .offset4 moves .col_4 over four columns.
```HTML
<div class="row">
    <div class="col_4 offset4"></div>
    <div class="col_8"></div>
</div><!-- /row -->
```

## Nesting columns
To nest your content with the default grid, add a new .row and set of .col_* columns within an existing .col_* column. Nested rows should include a set of columns that add up to the number of columns of its parent.
```HTML
<div class="row">
    <div class="col_4"></div>
    <div class="col_8">
        <div class="row">
            <div class="col_4"></div>
            <div class="col_4"></div>
      </div><!-- /row -->
  </div><!-- /col_8 -->
</div><!-- /row -->
```

## Fixed layout
When you want to make a layout fixed and most of the time center that div I always use ```<div class="container">``` for this.
```HTML
<body>
    <div class="container">

    </div>
</body>
```

