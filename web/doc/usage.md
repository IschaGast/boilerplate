# Usage

## Basic structure

```
.
+ doc
+ img
+ includes
+ static
  + /css/
    | style.css
  + /scss/
    + /base/
      | _settings.scss
      | _reset.scss
      | _colors.scss
    + /layout/
      | _grid.scss
    + /modules/
      | _box.scss
      | _button.scss
    + /non-modular/
      | _hacks.scss
    | style.scss
.htaccess
404.html
config.rb
favicon.ico
index.shtml
README.md
```

What follows is a general overview of each major part and how to use them.

### doc

This directory contains all the HTML5 Boilerplate documentation. You can use it
as the location and basis for your own project's documentation.

### img

This directory should contain all your project's content image files. Files that could be added by the content editor from the site.

### includes

This directory should contain all your project's content image files. Files that could be added by the content editor from the site.

#### _head.shtml
This can be removed when a site isn't going to be responsive.
```HTML
<meta name="viewport" content="width=device-width" />
```

### css

This directory should contain all your project's CSS files. It includes some
initial CSS to help get you started from a solid foundation. [About the
CSS](css.md).

### .htaccess

The default web server config is for Apache. [About the .htaccess](htaccess.md).

Host your site on a server other than Apache? You're likely to find the
corresponding configuration file in our [server configs
repo](https://github.com/h5bp/server-configs). If you cannot find a
configuration file for your setup, please consider contributing one so that
others can benefit too.

### 404.html

A helpful custom 404 to get you started.

### index.html

This is the default HTML skeleton that should form the basis of all pages on
your site. If you are using a server-side templating framework, then you will
need to integrate this starting HTML with your setup.

Make sure that you update the URLs for the referenced CSS and JavaScript if you
modify the directory structure at all.

If you are using Google Analytics, make sure that you edit the corresponding
snippet at the bottom to include your analytics ID.

### icons

Replace the default `favicon.ico` and apple touch icons with your own. You
might want to check out Hans Christian's handy [HTML5 Boilerplate Favicon and
Apple Touch Icon
PSD-Template](http://drublic.de/blog/html5-boilerplate-favicons-psd-template/).

[apple-touch-icons]